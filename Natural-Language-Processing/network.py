# Create a network of neural networks for NLP processing using the Reddit comments corpus. 
# This works by doing a simple classification of sentences by gathering sentences that contain 
# a randomly chosen word from a list of top-words of the corpus, and then applying this subset 
# as training data across an RNN node.
#
# USAGE:
#
# python network.py --filename data/reddit-comments.csv --top-words=20 --network-size=5
#
import os
import nltk
import string
import random
import glob
import subprocess as sp
from datetime import datetime
from argparse import ArgumentParser
from collections import OrderedDict


PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.join(PROJECT_DIR, 'data')


def top_words(filename, num, as_markdown=True):
    # Tokenize a file and sort it by the frequencies of the words
    # Print it as Markdown if necessary
    words = dict()
    lines = list()
    with open(filename) as csvfile:
        for line in csvfile:
            tokens = nltk.word_tokenize(line.lower().decode('utf-8'))
            lines.append(line.lower().decode('utf-8'))
            for tok in tokens:
                token = tok.encode('utf-8')
                token = token.translate(None, string.punctuation)  # remove all punctuation
                if len(token) > 0:
                    if token not in words:
                        words[token] = 1
                    else:
                        words[token] += 1
    
    sorted_words = OrderedDict(sorted(words.items(), key=lambda x: x[1], reverse=True))

    print 'Top %d words: \n' % num
    count = 1
    if as_markdown:
        print '| Word Number |    Token    | Frequency |'
        print '|------------:|------------:|----------:|'
    for word in sorted_words.keys()[:num]:
        if as_markdown:
            print '|%d|%s|%d|' % (count, word, words[word])
        else:
            print word, words[word]
        count += 1
    return sorted_words, lines


def sentences_with_word(lines, word):
    return [s for s in lines if word in s]


def build_network(args):
    # get the sorted words, and all the lines from the filename
    words, lines = top_words(args.filename, args.top_words)
    
    # obtain n random words (i.e. n = the network size) from the top words
    words_subset = random.sample(words.keys()[:args.top_words], args.network_size)

    # write the sentences from each into a separate file in the DATA_DIR
    for word in words_subset:
        with open(os.path.join(DATA_DIR, '%s.txt' % word), 'w') as outfile:
            sentences = sentences_with_word(lines, word)
            for sentence in sentences:
                outfile.write('%s\n' % sentence.encode('utf-8'))

    # now build a network (intermediate states are saved with the name of the word)
    training_begin = datetime.now()
    for node, word in enumerate(words_subset):
        print '\nTraining node %d ...' % node
        begin = datetime.now()
        cmd = ['python', 'train-theano.py', '-w', word]
        proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.STDOUT)
        for line in iter(proc.stdout.readline, b''):
            print '>>> ', line.rstrip()
        print '\nTotal time taken to train node %d: %s\n\n' % (node, datetime.now() - begin)
    
    print 'Training of %d nodes complete! Total runtime: %s' % (args.network_size, datetime.now() - begin)
    return words_subset


def query_network(word, num_sentences):
    # Query the network to see what possible output is generated
    # python load_model.py --model reddit-comments --training-data reddit-comments
    # first, load the last model trained
    files = filter(os.path.isfile, glob.glob(DATA_DIR + '/*.txt'))
    files.sort(key=lambda x: os.path.getmtime(x), reverse=True)  # sort the files by the modification time
    
    # at this point, our desired model (the last one) would be the first index
    model_filename = files[0]
    
    # now, load the model
    cmd = 'python load_model.py --model %s --training-data %s --num-sentences %d' % (model_filename, word, num_sentences)
    proc = sp.Popen(cmd.split(), stdout=sp.PIPE, stderr=sp.STDOUT)
    for line in iter(proc.stdout.readline, b''):
        print '>>> ', line.rstrip()


def main():
    parser = ArgumentParser()
    parser.add_argument('--filename', help='The name of the input file.')
    parser.add_argument('--top-words', help='The number of top words to obtain from the file.', type=int)
    parser.add_argument('--network-size', help='The size of the network to be built.', type=int, default=5)
    args = parser.parse_args()

    words_subset = build_network(args)

    
if __name__ == '__main__':
    main()
 
