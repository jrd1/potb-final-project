import csv
import nltk
import itertools
import numpy as np
from rnn_theano import RNNTheano
from argparse import ArgumentParser
from utils import load_model_parameters_theano, save_model_parameters_theano


unknown_token = "UNKNOWN_TOKEN"
sentence_start_token = "SENTENCE_START"
sentence_end_token = "SENTENCE_END"

vocabulary_size = 8000  # 8000
num_sentences = 7
senten_min_length = 5
hidden_dim = 10  # 50

parser = ArgumentParser()
parser.add_argument('--model', help='The name of the model you want to use.')
parser.add_argument('--training-data', help='The name of the training data file to use.')
parser.add_argument('--num-sentences', help='The number of sentences to be generated.', type=int, default=0)  # if 0, then the model is interactive
args = parser.parse_args()

print 'Loading training data "%s" ...' % args.training_data
if 'reddit' in args.training_data:
    training_data = './data/%s.csv' % args.training_data
    with open(training_data, 'rb') as f:
        reader = csv.reader(f, skipinitialspace=True)
        reader.next()
        # Split full comments into sentences
        sentences = itertools.chain(*[nltk.sent_tokenize(x[0].decode('utf-8').lower()) for x in reader])
        # Append SENTENCE_START and SENTENCE_END
        sentences = ["%s %s %s" % (sentence_start_token, x, sentence_end_token) for x in sentences]
else:
    training_data = './data/%s.txt' % args.training_data
    with open(training_data) as f:
        lines = f.readlines()
        sentences = itertools.chain(*[nltk.sent_tokenize(x.decode('utf-8').lower()) for x in lines])
        sentences = ['%s %s %s' % (sentence_start_token, x, sentence_end_token) for x in sentences]

tokenized_sentences = [nltk.word_tokenize(s) for s in sentences]
word_freq = nltk.FreqDist(itertools.chain(*tokenized_sentences))

 
# Get the most common words and build index_to_word and word_to_index vectors
vocab = word_freq.most_common(vocabulary_size-1)
index_to_word = [x[0] for x in vocab]
index_to_word.append(unknown_token)
word_to_index = dict([(w,i) for i,w in enumerate(index_to_word)])


def generate_sentence(model):
    # We start the sentence with the start token
    new_sentence = [word_to_index[sentence_start_token]]
    # Repeat until we get an end token
    while not new_sentence[-1] == word_to_index[sentence_end_token]:
        next_word_probs = model.forward_propagation(new_sentence)
        sampled_word = word_to_index[unknown_token]
        # We don't want to sample unknown words
        while sampled_word == word_to_index[unknown_token]:
            samples = np.random.multinomial(1, next_word_probs[-1])
            sampled_word = np.argmax(samples)
        new_sentence.append(sampled_word)
    sentence_str = [index_to_word[x] for x in new_sentence[1:-1]]
    return sentence_str


print 'Loading model: "%s" ...' % args.model

model = RNNTheano(vocabulary_size, hidden_dim=hidden_dim)
load_model_parameters_theano('./data/%s.npz' % args.model, model)

print 'Model loaded! Generating sentences'


doloop = True
while doloop:
    if args.num_sentences == 0:
        number_of_sentences = raw_input('How many sentences do you want to generate? : ')
    else:
        number_of_sentences = args.num_sentences

    for i in range(int(number_of_sentences)):
        sent = []
        # We want long sentences, not sentences with one or two words
        while len(sent) < senten_min_length:
            sent = generate_sentence(model)
        s = " ".join(sent)
        output = s
        print '\t"%s"' % output

    if args.num_sentences == 0:
        keep_looping = raw_input('Do you want to continue? (Y/n) : ')
        if keep_looping.strip() == 'n' or keep_looping.strip() == 'N':
            doloop = False
        print
        print
    else:
        doloop = False

